
#### Dots (or maybe *Connecto*)

A game that challenges you to connect as many dots together with non-diagonal lines. If
you create a closed connection (a loop), cool things will happen.

### Install

1. Pull down the bitbucket repository
2. Install Node
3. npm install -g 'coffee-script'
4. npm install -g 'nodemon'
5. cd ./dots
6. npm install -d
7. cake dev | cake run