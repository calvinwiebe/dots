
# Client side multiplayer menus and gameplay
# Note: socket.io doesn't seem to support require amd define style. So, don't use its exports directly
# as it will just attach itself to the window (i.e. bind its return to `notIo`)
define ['_', 'dots', 'eventbus', 'hammer', 'io'], (_, {Game}, eventbus, Hammer, notIo) ->

    class Menu
        tag: 'content'
        templateId: 'template-multiplayer-menu'

        constructor: (@results) ->
            _.bindAll this
            @template = _.template document.id(@templateId).innerHTML
            @render()
            @el = document.id('menu')
            @createButton = document.id('createButton')
            @joinButton = document.id('joinButton')
            @goMenuButton = document.id('goMenu')
            # Setup Handlers
            Hammer(@createButton).on 'tap', @onCreateTap
            Hammer(@joinButton).on 'tap', @onJoinTap
            Hammer(@goMenuButton).on 'tap', @onGoMenuTap

        onCreateTap: ->
            console.log 'create game tapped'
            eventbus.trigger 'attempCreateGame', { lobby: document.id('lobbyInput').value }

        onJoinTap: ->
            eventbus.trigger 'attemptJoinGame', { lobby: document.id('lobbyInput').value }

        onGoMenuTap: ->
            eventbus.trigger 'endMultiplayer'

        render: ->
            document.id('content').innerHTML = @template()

        destroy: ->
            Hammer(@createButton).off 'tap', @onCreateTap
            Hammer(@joinButton).off 'tap', @onJoinTap
            Hammer(@goMenuButton).off 'tap', @onGoMenuTap

    class ReadyUp
        tag: 'content'
        templateId: 'template-readyup'

        constructor: ->
            _.bindAll this
            @template = _.template document.id(@templateId).innerHTML
            @state = 'Ready up!'
            @render()
            @readyButton = document.id('readyButton')
            # Setup Handlers
            Hammer(@readyButton).on 'tap', @onReadyUp

        onReadyUp: ->
            eventbus.trigger 'readiedUp'

        wait: ->
            @state = 'Waiting...'
            @readyButton.innerHTML = @state

        render: ->
            document.id('content').innerHTML = @template()

        destroy: ->
            Hammer(@readyButton).off 'tap', @onReadyUp


    class RealTimeGame extends Game

        templateId: 'template-multiplayer-game'

        constructor: (@socket, mode) ->
            @opponentScore = 0
            super mode
            # setup an interval to notify the server/other player
            # of our current score
            @scoreSyncTicker = setInterval @syncScore, 4000
            # listen for our opponent's score
            @socket.on "opponentScoreSync", @updateOpponentScore

        syncScore: ->
            @socket.emit "scoreSync", { @score }

        updateOpponentScore: (data) ->
            @opponentScore = data.score
            @render()

        onGameEnd: ->
            @grid.disable()
            clearInterval @scoreSyncTicker
            @state = 'gameOver'
            eventbus.trigger 'multiPlayerGameOverInternal'

        render: ->
            super()
            opponentScore = document.id 'opponentScore'
            opponentScore.innerHTML = @opponentScore

        destroy: ->
            super()
            @socket.removeAllListeners "opponentScoreSync"

    class MultiplayerGame
        constructor: ->
            _.bindAll this
            @state = 'lobby'
            @menu = new Menu
            @socket = io.connect('/')

            @socket.on 'readyUp', @readyUp
            @socket.on 'play', @onPlay

            eventbus.on 'attempCreateGame', @attempCreateGame
            eventbus.on 'attemptJoinGame', @attemptJoinGame
            eventbus.on 'readiedUp', @onReadyUp
            eventbus.on 'multiPlayerGameOverInternal', @onGameOver

        attempCreateGame: (data) ->
            @socket.emit 'createGame', lobby: data.lobby

        attemptJoinGame: (data) ->
            @socket.emit 'joinGame', lobby: data.lobby

        readyUp: ->
            @state = 'readyUp'
            @menu.destroy()
            @menu = null
            @readyUpScreen = new ReadyUp

        onReadyUp: ->
            @state = 'ready'
            @readyUpScreen.wait()
            @socket.emit 'readiedUp'

        onPlay: (data) ->
            # we need to get the `data.grid` here, JSON.parse it
            # and init our RealTimeGame with it
            @readyUpScreen.destroy()
            @readyUpScreen = null
            @game = new RealTimeGame @socket, 'timed'

        onGameOver: ->
            console.log 'this is now happening'
            @socket.on 'finalScoreUpdate', ({ you, them }) ->
                eventbus.trigger 'multiPlayerGameOver', { you, them }
            @socket.emit 'finalScore', score: @game.score

        destroy: ->
            @game.destroy()
            @game = null
            eventbus.off 'attempCreateGame', @attempCreateGame
            eventbus.off 'attemptJoinGame', @attemptJoinGame
            eventbus.off 'readiedUp', @onReadyUp
            eventbus.off 'multiPlayerGameOverInternal', @onGameOver
            @socket.removeAllListeners 'readyUp'
            @socket.removeAllListeners 'play'
            @socket.removeAllListeners 'finalScoreUpdate'
            @socket.emit 'leave'


    return {
        MultiplayerGame
    }