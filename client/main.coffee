
# Configure require to load in some resources from a cdn, so they can be
# included in the defines of other scripts
requirejs.config
    paths:
        # this needs to be a lowercase, as Hammer.js explicity defines its module
        # name for requirejs as `hammer`. See bottom of Hammer.js file
        hammer: "http://cdnjs.cloudflare.com/ajax/libs/hammer.js/1.0.5/hammer.min"
        _: "http://cdnjs.cloudflare.com/ajax/libs/lodash.js/1.3.1/lodash.min"
        io: "/socket.io/socket.io"


# Use require.js to grab our dependencies
require ['lib/domReady', 'menu'], (domReady, {Results, Menu, App}) ->

    # create some shorthands
    document.id = document.getElementById
    document.class = document.getElementsByClassName

    # Use require.js' domReady module for cross browser
    # DOMContentLoadedness (wouldn't work with require.js + window.onload here)
    domReady -> new App

