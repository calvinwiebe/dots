
# Setup the module and return the exported objects

define ['hammer', '_', 'lib/polyfill', 'lib/svg', 'eventbus'], (Hammer, _, notPollyfill, SVG, eventbus) ->

    colours = ['red', 'green', 'purple', 'orange', 'blue']

    getRandomInt = (upperBound) ->
        Math.floor(Math.random()*(new Date()).getTime() % upperBound)

    easeBounce = (pos) ->
        s = 7.5625
        p = 2.75
        if (pos < (1/p))
            l = s * Math.pow(pos, 2)
        else
            if (pos < (2/p))
                pos -= 1.5/p
                l = s * Math.pow(pos, 2) + 0.75
            else
                if (pos < 2.5/p)
                    pos -= 2.25/p
                    l = s * Math.pow(pos, 2) + 0.9375
                else
                    pos -= 2.625/p
                    l = s * Math.pow(pos, 2) + 0.984375
        return l


    class Dot

        constructor: (@canvas, @coordinates, @diameter, @column, @row) ->
            _.bindAll this
            @state = 'listening'
            @selected = false
            @colour = colours[getRandomInt(colours.length)]
            @gradient = @canvas.gradient 'radial', (stop) =>
                stop.at { offset: 0.5, color: @colour }
                stop.at { offset: 1, color: '#fff' }

            @id = getRandomInt 100000000000

            @el = @canvas.circle(@diameter).center(@coordinates.x, @coordinates.y-@diameter*10)
            @el.attr(
                fill: @colour,
                stroke: 'white',
                'stroke-width': @diameter / 4
            )
            @moveDot row, @coordinates.y

            # setup handlers
            @el.node.addEventListener 'indirectDrag', @onDrag
            Hammer(@el.node, prevent_default: true).on 'doubletap', @onDoubleTap

        highlight: (disable) ->
            if disable then color = 'white' else color = @gradient
            @el.attr stroke: color

        moveDot: (@row, y) ->
            @coordinates.y = y
            @el.animate(750, easeBounce).attr(cy: y)

        setColour: (colour) ->
            @colour = colour
            @el.attr fill: @colour

        listen: ->
            @state = 'listening'

        stopListening: ->
            @state = 'neutral'

        onDrag: (event) ->
            if @state is 'listening'
                @touch()

        onDoubleTap: (htEvent) ->
            @destroy()
            eventbus.trigger 'dotSquished', this

        touch: ->
            @state = 'neutral'

            # create another circle behind existing one to do the flash effect
            flash = @canvas.circle(@diameter).center(@coordinates.x, @coordinates.y).attr(fill: @colour)
            flash.front()
            flash.animate(500, '>').attr(rx: @diameter*0.75, ry: @diameter*0.75, opacity: 0).after ->
                flash.remove()

            eventbus.trigger 'dotTouched', this

        select: ->
            @selected = true
            @highlight()

        deselect: ->
            @selected = false
            @highlight true

        destroy: ->
            @state = 'deleted'

            eventbus.trigger 'dotDestroyed'

            @el.node.removeEventListener 'indirectDrag', @onDrag
            Hammer(@el.node).off 'doubleTap', @onDoubleTap

            @el.animate(200, '>').attr(rx: 0, ry: 0).after (effect) ->
                effect.target.remove()

        isSameColor: (otherDot) -> @colour is otherDot.colour


    class DotCollection

        constructor: ->
            @dots = []
            @dotsHash = {}

            # extend ourselves with the lodash wrapped dotsHash object's prototype.
            # This will enable us to perform lodash operations directly on dotsHash through
            # `this`. Ex.
            # dc = new DotsCollection
            # dc.each (dot) -> dot.colour 'blue'
            lo = _(@dotsHash)
            _.extend this, lo, Object.getPrototypeOf lo

        add: (column, row, dot) ->
            @dots[column] ?= []
            @dots[column][row] = dot

            # add to dot hash for easy filtering/mapping/iterating
            @dotsHash[dot.id] = dot

        remove: (column, row) ->
            dot = @dots[column][row]
            delete @dotsHash[dot.id]
            @dots[column][row] = null


        getNeighbours: (dot) ->
            neighbours = []

            neighbours.push @dots[dot.column][dot.row + 1] if dot.row + 1 < @dots[dot.column].length
            neighbours.push @dots[dot.column][dot.row - 1] if dot.row - 1 >= 0

            neighbours.push @dots[dot.column + 1][dot.row] if dot.column + 1 < @dots.length
            neighbours.push @dots[dot.column - 1][dot.row] if dot.column - 1 >= 0

            return neighbours

        getConnectableNeighbours: (dot) ->
            connectableNeighbours = []
            for neighbour in @getNeighbours(dot)
                if dot.isSameColor(neighbour)
                    connectableNeighbours.push neighbour
            return connectableNeighbours

        isCellEmpty: (column, row) ->
            return !@dots[column] || !@dots[column][row]

        moveDot: (dot, newRow, newY) ->
            @dots[dot.column][dot.row] = null
            @dots[dot.column][newRow] = dot

            dot.moveDot newRow, newY


    class Line

        constructor: (canvas, args) ->
            @dotA = args.dotA
            @colour = @dotA.colour
            @dotB = args.dotB
            @coordsB = args.toCoordinates or @dotB.coordinates

            @line = canvas.polyline([[@dotA.coordinates.x, @dotA.coordinates.y], [@coordsB.x, @coordsB.y]]).attr(
                'stroke-width': @dotA.diameter / 6,
                'stroke-opacity': 0.75,
                stroke: @colour
            )

        drawLine: ->
            @line.plot([[@dotA.coordinates.x, @dotA.coordinates.y], [@coordsB.x, @coordsB.y]])

        updateLinePivot: (@dotA) ->
            @drawLine()

        updateLineTarget: (@coordsB) ->
            @drawLine()

        isConnecting: (dotA, dotB) ->
            return @hasDot(dotA) and @hasDot(dotB)

        hasDot: (dot) ->
            return @dotA is dot or @dotB is dot

        destroy: ->
            @line.remove()


    class Grid

        constructor: () ->
            _.bindAll this
            @state = 'neutral'
            @loops = []
            @dots = []
            @lines = []
            @touchCoords = x: 0, y: 0
            @gridSize = 6
            @innerPadding = 0.5
            @outerPadding = 1
            svgContainer = document.id 'svgContainer'
            container = document.id 'container'
            canvasEl = document.id 'canvas'
            style = window.getComputedStyle canvasEl, null
            height = parseInt style.getPropertyValue('height'), 10
            width = parseInt style.getPropertyValue('width'), 10

            # d (x + i(x-1) + 2o)
            # d (x(1 + i) - i + 2o)
            @dotDiameter = Math.floor((Math.min(height, width)) / (@gridSize * (1 + @innerPadding) - @innerPadding + 2 * @outerPadding))
            @gridWidth = @dotDiameter * (@gridSize * (1 + @innerPadding) - @innerPadding + 2 * @outerPadding)
            @gridHeight = @dotDiameter * (@gridSize * (1 + @innerPadding) - @innerPadding + 2 * @outerPadding)

            @canvas = SVG(svgContainer).size(@gridWidth, @gridHeight)

            @offsetLeft = container.offsetLeft + @canvas.node.offsetLeft
            @offsetTop = canvasEl.offsetTop

            @dots = new DotCollection()

            @init()

            #event handlers
            Hammer(@canvas.node, prevent_default: true, drag_min_distance: 1).on 'drag', @onDrag
            Hammer(@canvas.node).on 'release', @onRelease
            eventbus.on 'dotTouched', @onDotTouched
            eventbus.on 'dotSquished', @onDotSquished

        init: ->
            @fillGrid()
            eventbus.trigger 'gridInitialized'

        disable: ->
            @dots.each (dot) ->
                dot.setColour 'gray'
                dot.stopListening()
            @state = 'disabled'

        convertCRtoXY: (column, row) ->
            # d (x(1 + i) + o + 0.5)
            x = @dotDiameter * (column * (1 + @innerPadding) + @outerPadding + 0.5)
            y = @gridHeight - @dotDiameter * (row * (1 + @innerPadding) + @outerPadding + 0.5)
            { x, y }

        fillGrid: ->
            for column in [0...@gridSize]
                numDeleted = 0
                for row in [0...@gridSize]
                    if @dots.isCellEmpty column, row
                        @createDot column, row
                    else if @dots.dots[column][row].state is 'deleted'
                        numDeleted++
                        @dots.remove(column, row)
                    else if numDeleted > 0
                        @dots.moveDot @dots.dots[column][row], row - numDeleted, @convertCRtoXY(column, row - numDeleted).y
                if numDeleted
                    while numDeleted
                        @createDot column, @gridSize-numDeleted--

        createDot: (column, row) ->
            dot = new Dot @canvas, @convertCRtoXY(column, row), @dotDiameter, column, row
            @dots.add column, row, dot

        onDotTouched: (dot) ->
            @touchCoords = dot.coordinates

            switch @state
                when 'neutral'
                    @state = 'connecting'

                    @currentDot = dot
                    @currentLine = new Line @canvas, dotA: dot, toCoordinates: @touchCoords

                    requestAnimationFrame =>
                        @moveLineSegment()

                    @dots.each (dot) -> dot.stopListening() if not dot.selected
                    neighbour.listen() for neighbour in @dots.getConnectableNeighbours(dot)

                when 'connecting'
                    #undoing a line
                    if @lines[@lines.length - 1]?.isConnecting @currentDot, dot
                        @lines.pop().destroy()
                        #check if the line we are deleting is the one that made the first loop
                        deselect = true
                        for lineIndex in @loops
                            if lineIndex is @lines.length
                                deselect = false
                                if lineIndex is @loops[0]
                                    @loops = []
                                    @dots.each (dot) => dot.highlight true if dot.isSameColor(@currentDot) and dot.selected is false
                                    break
                        if deselect
                            @currentDot.deselect()
                    else
                        @lines.push new Line @canvas, dotA: @currentDot, dotB: dot
                        if dot.selected
                            @loops.push @lines.length - 1
                            if @loops.length is 1
                                @dots.each (dot) => dot.highlight() if dot.isSameColor @currentDot

                    @currentLine.updateLinePivot dot

                    # turn off drag listen for neighbour of previous dots
                    neighbour.stopListening() for neighbour in @dots.getConnectableNeighbours(@currentDot)
                    @currentDot = dot
                    neighbour.listen() for neighbour in @dots.getConnectableNeighbours(dot)

            @currentDot.select()

        onDotSquished: (dot) ->
            @fillGrid()
            eventbus.trigger 'moveMade'

        onDrag: (htEvent) ->
            indirectDragEvent = document.createEvent 'CustomEvent'
            indirectDragEvent.initCustomEvent 'indirectDrag', true, true, event: htEvent
            # no drag event targets are updated, Hammer issue #269
            targets = document.elementsFromPoint htEvent.gesture.center.pageX, htEvent.gesture.center.pageY, ['ellipse','polyline']
            target.dispatchEvent indirectDragEvent for target in targets

            if @state is 'connecting'
                @touchCoords =
                    x: htEvent.gesture.center.pageX - @offsetLeft,
                    y: htEvent.gesture.center.pageY - @offsetTop

        onRelease: (htEvent) ->
            if @state is 'connecting'
                @state = 'neutral'

                @currentLine.destroy()

                if @lines.length > 0
                    if @loops.length
                        @dots.each (dot) => dot.destroy() if dot.isSameColor @currentDot
                        @loops = []
                    else
                        @dots.each (dot) => dot.destroy() if dot.selected
                    line.destroy() for line in @lines
                    @lines = []
                    @fillGrid()
                    # this is a move, let the rest of the app know
                    eventbus.trigger 'moveMade'
                else
                    @currentDot.deselect()

                @dots.each (dot) -> dot.listen()

        moveLineSegment: ->
            if @state is 'connecting'
                requestAnimationFrame =>
                    @moveLineSegment()
                @currentLine.updateLineTarget @touchCoords

        destroy: ->
            eventbus.off 'dotTouched', @onDotTouched
            eventbus.off 'dotSquished', @onDotSquished
            Hammer(@canvas.node).off 'drag', @onDrag
            Hammer(@canvas.node).off 'release', @onRelease


    class GameTicker

        text:
            'timed': 'Time'
            'moves': 'Moves'

        constructor: (@mode, {@seconds, @moves}) ->
            _.bindAll this

            switch @mode
                when 'timed'
                    @ticksLeft = @seconds
                    @ticker = setInterval @onTick, 1000
                when 'moves'
                    @ticksLeft = @moves
                    eventbus.on 'moveMade', @onTick

            @el = document.id 'ticker'
            @el.innerHTML = "#{@text[@mode]}: #{@ticksLeft}"

        onTick: ->
            if --@ticksLeft is 0
                clearInterval @ticker if @ticker?
                eventbus.trigger 'gameEnd'

            @el.innerHTML = "#{@text[@mode]}: #{@ticksLeft}"

        destroy: ->
            eventbus.off 'moveMade'

    class Game

        levelOptions:
            timed:
                1: 15
                2: 60
            moves:
                1: 5
                2: 20

        tag: 'content'
        templateId: 'template-game'

        constructor: (@mode) ->
            _.bindAll this
            @template = _.template document.id(@templateId).innerHTML

            # set up event handlers before creating a grid
            eventbus.on 'dotDestroyed', @onDotDestroyed
            eventbus.on 'gridInitialized', @startGame
            eventbus.on 'gameEnd', @onGameEnd
            @state = 'initializing'
            @level = 1
            @score = 1
            @render()

            @grid = new Grid()

        onDotDestroyed: () ->
            @score++
            @render()

        startGame: ->
            options = {
                seconds: @levelOptions.timed[@level]
                moves: @levelOptions.moves[@level]
            }
            switch @mode
                when 'timed'
                    @gameTicker = new GameTicker @mode, options
                when 'moves'
                    @gameTicker = new GameTicker @mode, options
            @state = 'playing'

        onGameEnd: ->
            @grid.disable()
            @state = 'gameOver'
            eventbus.trigger 'gameOver', { @score }

        render: ->
            if not @rendered
                document.id(@tag).innerHTML = @template { @score }
                @rendered = true
            @scoreElement = document.id 'score'
            @scoreElement.innerHTML = @score

        destroy: ->
            @grid.destroy()
            @gameTicker.destroy()
            eventbus.off 'dotDestroyed'
            eventbus.off 'gridInitialized'
            eventbus.off 'gameEnd'


    # Will get all layered elements that are under the
    # specified coordinates. It will return all elements on top of
    # the body by default. If `tags` is specified it will continue to find
    # elements of those tags, until it finds one that isn't and bails out.
    document.elementsFromPoint = (args...) ->
        tags = args[2] if args.length is 3
        x = args[0]
        y = args[1]
        elems = []
        elem = document.elementFromPoint x, y
        while (-> if tags? then elem?.tagName in tags else elem?.tagName isnt 'BODY')()
            elems.push elem
            elem.style.display = 'none'
            elem = document.elementFromPoint x, y
        elem.style.display = 'inherit' for elem in elems
        elems

    # Exports visible to requirejs
    return {
        Game
    }
