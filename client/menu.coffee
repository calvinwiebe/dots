
define ['hammer', '_', 'eventbus', 'dots', 'persistence', 'multiplayer'], (Hammer, _, eventbus, {Game}, {scores, preferences}, {MultiplayerGame}) ->

    class Results
        tag: 'content'
        templateId: 'template-results'

        constructor: (@options) ->
            _.bindAll this
            @template = _.template document.id(@options.template ? @templateId).innerHTML
            @render()
            @el = document.id('options')
            @menuButton = document.id('go-menu')
            @startButton = document.id('start')
            # Setup Handlers
            Hammer(@menuButton).on 'tap', @onMenuTap
            Hammer(@startButton).on 'tap', @onStartTap

        onMenuTap: ->
            eventbus.trigger 'resultsExit'

        onStartTap: ->
            eventbus.trigger 'startGame', { type: preferences.getUserPreferences().gameMode }

        render: ->
            highScore = @options.scores?.shift(0) ? 0
            document.id('content').innerHTML = @template {
                highScore
                current: @options.current ? 0
                scores: @options.scores?.slice(0, 3)
                opponentScore: @options.opponentScore
            }

        destroy: ->
            Hammer(@menuButton).off 'tap', @onMenuTap
            Hammer(@startButton).off 'tap', @onStartTap

    class Menu
        tag: 'content'
        templateId: 'template-menu'

        constructor: ->
            _.bindAll this
            @template = _.template document.id(@templateId).innerHTML
            @type = preferences.getUserPreferences().gameMode
            @playImmediate = preferences.getUserPreferences().playImmediate
            @render()
            @el = document.id("menu")
            @startButton = document.id("start")
            @multiplayerButton = document.id('multiplayer')
            @fieldset = document.id('gameType')
            @playImmediateEl = document.id('play-immediate')
            # Setup Handlers
            Hammer(@startButton).on 'tap', @onStartTap
            Hammer(@multiplayerButton).on 'tap', @onMultiplayerTap
            @fieldset.addEventListener 'change', @onTypeChange
            @playImmediateEl.addEventListener 'change', @onPlayImmediateChange

        onTypeChange: (e) ->
            @type = e.target.value
            preferences.setUserPreferences gameMode: @type

        onPlayImmediateChange: (e) ->
            @playImmediate = e.target.checked
            preferences.setUserPreferences { @playImmediate }

        onStartTap: ->
            eventbus.trigger 'startGame', { @type }

        onMultiplayerTap: ->
            eventbus.trigger 'multiplayer'

        render: ->
            document.id(@tag).innerHTML = @template { @type, @playImmediate }

        destroy: ->
            Hammer(@startButton).off 'tap', @onStartTap
            Hammer(@multiplayerButton).off 'tap', @onMultiplayerTap
            @fieldset.removeEventListener 'change', @onTypeChange
            @playImmediateEl.removeEventListener 'change', @onPlayImmediateChange

    class App
        constructor: ->
            _.bindAll this
            @preferences = preferences.getUserPreferences()

            if @preferences.playImmediate
                @game = new Game @preferences.gameMode
            else
                @menu = new Menu

            # setup handlers for game transitions
            # Menu -> Game -> Results -> Game
            #                 Results -> Menu
            # Menu -> MultiplayerMenu -> MultiPlayerGame -> MultiplayerMenu
            #                         -> Menu
            eventbus.on 'startGame', (data) =>
                @results?.destroy()
                @results = null
                @menu?.destroy()
                @menu = null
                @game = new Game data.type

            eventbus.on 'gameOver', @onGameOver

            eventbus.on 'resultsExit', (data) =>
                @results.destroy()
                @results = null
                @menu = new Menu

            eventbus.on 'multiplayer', =>
                # pass control to the multiplayer game
                eventbus.off 'gameOver', @onGameOver
                eventbus.on 'multiPlayerGameOver', @onMultiGameOver
                @menu.destroy()
                @menu = null
                @multiplayerGame = new MultiplayerGame

        onGameOver: (results) ->
            @game.destroy()
            @game = null
            scores.saveScore results.score
            @results = new Results { current: results.score, scores: scores.getScores() }

        onMultiGameOver: (results) ->
            @multiplayerGame.destroy()
            @multiplayerGame = null
            eventbus.off 'multiPlayerGameOver', @onMultiGameOver
            eventbus.on 'gameOver', @onGameOver
            @results = new Results
                template: 'template-multiplayer-results'
                current: results.you
                opponentScore: results.them



    return {
        Results
        Menu
        App
    }
