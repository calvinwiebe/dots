
define ['_'], (_) ->

    # localStorage shims
    # ------------------

    ls = ->
        try
            localStorage.setItem 'fakesies', '{}'
            localStorage.removeItem 'fakesies'
            localStorage
        catch err
            if not window.noLocalStorageAlert?
                alert 'It looks like you are in some sort of private browsing mode. We cannot ' +
                      'save or load your preferences or scores in this mode.'
                window.noLocalStorageAlert = true
            return {
                getItem: (key) -> null
                setItem: (key, value) -> null
            }

    # Preferences
    # -----------

    VALID_ENTRIES = [
        'gameMode'
        'playImmediate'
        #'colourTheme'
    ]

    DEFAULTS =
        gameMode: 'moves'
        playImmediate: false

    preferencesKey = 'dotPreferences'

    # save some user preferences
    # entries that are used:
    #
    #  * `gameMode` - "timed" or "moves"
    #  * `playImmediate` - used at startup to determine if the user would like to jump into
    #     a game with their saved preferences immediately.
    #  * `colourTheme` (not implemented) - the colour theme of the dots
    setUserPreferences = (prefs={}) ->
        # get current
        currentPreferences = getUserPreferences()
        currentPreferences ?= DEFAULTS
        # blow away any entries that should not be saved
        delete prefs[invalid] for invalid in _.reject _.keys(prefs), (key) -> key in VALID_ENTRIES
        # merge the ones being set with the current (overwriting current with new)
        prefs = _.extend currentPreferences, prefs
        # save
        ls().setItem preferencesKey, JSON.stringify prefs
        # return prefs that were saved
        prefs

    getUserPreferences = ->
        prefs = JSON.parse ls().getItem preferencesKey
        prefs ?= DEFAULTS
        prefs

    # Scores
    # ------

    scoresKey = "scores"

    getScores = ->
        scores = JSON.parse(ls().getItem scoresKey)
        scores ?= [0,0,0,0]
        # return sorted highest to lowest
        scores.sort (a, b) -> b - a

    saveScore = (score) ->
        currentScores = JSON.parse(ls().getItem scoresKey)
        currentScores ?= []
        currentScores.push score
        ls().setItem scoresKey, JSON.stringify(currentScores)

    return {
        scores:
            getScores: getScores
            saveScore: saveScore
        preferences:
            setUserPreferences: setUserPreferences
            getUserPreferences: getUserPreferences
    }
