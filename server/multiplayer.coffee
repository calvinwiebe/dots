
# Multiplayer Events
# ------------------

# module scoped helpers
generateUniqueId = (collection) ->
    id = Math.round((Math.random()*100000),6)
    while id of collection
        id = Math.round((Math.random()*100000),6)
    id

# Player
# a player is associated with a socket. A player has instance methods
# to perform player-type actions on its socket.
# All socket events are defined in this class except for the 'finalScore' event. This
# is handled by the gameManager
class Player

    constructor: (@socket, @connectionData) ->
        _.bindAll this
        @state = 'idle'
        @setupListenersForState()
        # setup some always on listeners
        @socket.on 'disconnect', @onDisconnect
        @socket.on 'leave', @onLeave

    stopListening: (events) ->
        @socket.removeAllListeners event for event in events

    setupListenersForState: ->

        # blow away existing listeners
        @stopListening [
            'readiedUp'
            'scoreSync'
            'gameEnd'
        ]

        switch @state
            # when 'idle'
                # we are talked to directly by the gameManager here
            when 'readyUp'
                @socket.on 'readiedUp', @onReadiedUp
            # when 'ready'
                # we are talked to directly by the gameManager here
            when 'playing'
                @socket.on 'scoreSync', @relayScore
                @socket.on 'gameEnd', @onGameEnd

    chatMyClient: (message) ->
        @socket.emit 'chat', message: message

    relayOpponentsScore: (score) ->
        @socket.emit 'opponentScoreSync', { score }

    sendChatToOpponent: (data) ->
        @opponent.chatMyClient data.message

    sendConnectionResult: (result) ->
        @socket.emit 'connectionResult', result

    registerOpponent: (@opponent) ->
        @socket.on 'chat', @sendChatToOpponent

    removeOpponent: ->
        @socket.removeAllListeners 'chat', @sendChatToOpponent
        @opponent = null

    readyUp: ->
        @state = 'readyUp'
        @socket.emit 'readyUp'
        @setupListenersForState()

    onReadiedUp: ->
        @state = 'readiedUp'
        eventbus.trigger 'playerReady', this

    play: ->
        @state = 'playing'
        @setupListenersForState()
        @socket.emit 'play'

    relayScore: (data) ->
        @opponent?.relayOpponentsScore data.score

    onGameEnd: -> eventbus.trigger 'playerDisconnect', { manner: 'graceful', player: this }

    onDisconnect: -> eventbus.trigger 'playerDisconnect', { manner: 'disconnect', player: this }

    onLeave: -> eventbus.trigger 'playerDisconnect', { manner: 'graceful', player: this }

    destroy: -> @socket.removeAllListeners()


# PlayerManager
# A simple manager to perform administrative tasks
# on a list of players
class PlayerManager

    constructor: ->
        _.bindAll this
        @players = {}
        eventbus.on 'playerDisconnect', @cleanup

    add: (player) ->
        # create a unique id for the player
        id = generateUniqueId @players
        player.id = id
        @players[id] = player

    getPlayer: (id) ->
        return @players[id]

    cleanup: ({manner, player}) ->
        delete @players[player.id]
        # make the opponent the host
        if player.opponent?
            player.opponent.removeOpponent()
            player.opponent.role = 'host'
        player.destroy()
        eventbus.trigger 'playersRoleChange', player, player.opponent
        if manner is 'graceful'
            debugger
            createLobbySocket player.socket

    onPlayerGameOver: (player) ->


    destroy: ->
        eventbus.off 'playerDisconnect', @cleanup

# Game
# A Game maintains the state of a multiplayer game, and listens for in game
# socket.io events and acts upon them.
class Game
    constructor: ->
        _.bindAll this
        @state = 'idle'
        @host = null
        @guest = null
        @players = []
        eventbus.on 'playerReady', @onPlayerReady

    add: (player) ->
        @players.push player
        if player?.role is 'host'
            @host = player
            @state = 'hosted'
        else if player?.role is 'guest'
            @guest = player
            # we should have two players now, but check anyway
            if @host? and @guest?
                @state = 'readyUp'
                @host.registerOpponent @guest
                @guest.registerOpponent @host
                @sendReadyStatus()

    sendReadyStatus: ->
        _(@players).forEach (player) ->
            player.readyUp()

    onPlayerReady: (player) ->
        # check that both are ready
        # FIXME - make players EventEmitters themselves so we can bind just to
        # ours players. Right now we get all events from eventbus. Not good.
        if @host?.state is 'readiedUp' and @guest?.state is 'readiedUp'
            _(@players).forEach (player) -> player.play()

    isNotFull: ->
        return not @host? or not @guest?

    destroy: ->
        @players = null
        eventbus.off 'playerReady', @onPlayerReady

# GameManager
# A manager to handle the adding/joining of players to games. The game manager also
# handles deleting a game, and handling players disconnecting in a graceful manner.
class GameManager

    constructor: ->
        _.bindAll this
        @games = {}
        eventbus.on 'playersRoleChange', @cleanup

    add: (host, id) ->
        if id of @games
            result =
                successful: false
                game: id
                message: "a game with name #{id} already exists"
        else
            host.role = 'host'
            @games[id] = new Game
            @games[id].add host
            result =
                successful: true
                game: id
                message: "joined an empty game with id #{id}"

    join: (guest, id) ->
        # attempt to join an existing game
        game = @games[id]
        if game? and game.isNotFull()
            guest.role = 'guest'
            game.add guest
            @listenForFinalScore(id)
            result =
                successful: true
                game: id
                message: "successfully joined game #{id}"
        else
            result =
                successful: false
                message: 'game does not exist or game is full'

    getGameIdOfPlayer: (player) ->
        id = _.findKey @games, (game) -> _.contains game, player

    listenForFinalScore: (id) ->
        hostSocket = @games[id].host.socket
        guestSocket = @games[id].guest.socket

        do ->
            finalScores = {}

            scoreUpdated = ->
                if finalScores.host? and finalScores.guest?
                    hostSocket.emit 'finalScoreUpdate',
                        you: finalScores.host
                        them: finalScores.guest
                    guestSocket.emit 'finalScoreUpdate',
                        you: finalScores.guest
                        them: finalScores.host

            hostSocket.on 'finalScore', (data) ->
                hostSocket.removeAllListeners 'finalScore'
                finalScores.host = data.score
                scoreUpdated()

            guestSocket.on 'finalScore', (data) ->
                guestSocket.removeAllListeners 'finalScore'
                finalScores.guest = data.score
                scoreUpdated()

    cleanup: (leavee, opponent) ->
        # just delete the game
        id = @getGameIdOfPlayer leavee
        @games[id]?.destroy()
        delete @games[id] if id?
        if opponent?
            @add opponent, id

    destroy: ->
        @games = null
        eventbus.off 'playerRolesChange', @cleanup


createLobbySocket = (socket) ->

    createGame = (data) -> create 'create', socket, data

    joinGame = (data) -> create 'join', socket, data

    create = (method, socket, data) ->
        player = new Player socket, data
        playerManager.add player
        switch method
            when 'create' then result = gameManager.add player, data.lobby
            when 'join' then result = gameManager.join player, data.lobby

        player.sendConnectionResult result
        if result.successful
            socket.removeListener 'createGame', createGame
            socket.removeListener 'joinGame', joinGame

    # initially all sockets are 'lobby' sockets
    # just waiting for a lobby name, or waiting to immediately
    # join a game
    socket.on 'createGame', createGame
    socket.on 'joinGame', joinGame


# create a PlayerManager on require
playerManager   = null
gameManager     = null
# save a reference to the socket.io object
io              = null
_               = require 'lodash'
eventbus        = require '../shared/eventbus'

# initialize the socket.io connection handlers
exports.start = start = (ioRef) ->
    playerManager   = new PlayerManager
    gameManager     = new GameManager
    io              = ioRef

    # setup the initial handlers of a socket
    io.sockets.on 'connection', createLobbySocket

