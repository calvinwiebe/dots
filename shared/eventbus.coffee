
# Generates an instance of an EventEmitter or EventBus. It is
# shared by both the client and server to maintain one api
# In Node:
#   We use EventEmitter, as it is built in, as is fully featured.
# In browser:
#   We use microevent. It is a small script, as does what we need.

# determine what environment we are in

# We are in node.
if module?.exports? and global?
    EventEmitter = require('events').EventEmitter
    eventbus = new EventEmitter
    # map the EventEmitter functions to that of microevent to maintain the api
    eventbus.off = eventbus.removeListener
    eventbus.trigger = eventbus.emit
    module.exports = eventbus

# we are in the browser with require.js
else if typeof define is 'function' and define.amd?
    define ['lib/microevent'], (microevent) ->
        eventbus = {}
        microevent.mixin eventbus
        return eventbus