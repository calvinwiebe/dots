
# load in coffee-script so we can require coffee files
express     = require('express')
routes      = require('./routes')
http        = require('http')
path        = require('path')

app         = express()
server      = http.createServer(app)
# set up socket.io to intercept from express
io          = require('socket.io').listen(server)
multiplayer = require('./server/multiplayer')

# all environments
app.set('port', process.env.PORT || 3000)
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(express.favicon())
app.use(express.logger('dev'))
app.use(express.bodyParser())
app.use(express.methodOverride())
app.use(app.router)
app.use(express.static(path.join(__dirname, 'public')))

# development only
if 'development' == app.get('env')
  app.use express.errorHandler()

# define the routes

#basic game
app.get '/', routes.index

# start the multiplayer server service
#
# Since we are going to be using socket.io for interaction between players and
# the server for game moves/data syncing/etc, we can also use socket.io instead
# of explicit ajax post routes via express. i.e. All communication to do with multiplayer events
# is done through socket.io
multiplayer.start io

server.listen app.get('port'), ->
  console.log('Express server listening on port ' + app.get('port'))

