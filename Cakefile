
# Dots Cakefile
# -------------
#
# Will compile all coffee files in client/ in the public/javascripts
# and will run express.

fs = require 'fs'

{print} = require 'sys'
{spawn} = require 'child_process'

getSpawn = (cmd, args) ->
    if process.platform is 'win32'
        args = cmd + ' ' + args.join ' '
        spawned_cmd = spawn 'cmd', ['/c', args]
    else
        spawned_cmd = spawn cmd, args

    spawned_cmd

build = (callback) ->
    clientCoffees = getSpawn 'coffee', ['-c', '-b', '-o', 'public/javascripts', 'client']
    sharedCoffees = getSpawn 'coffee', ['-c', '-b', '-o', 'public/javascripts', 'shared']

    [clientCoffees, sharedCoffees].forEach (spawnInstance) ->
        spawnInstance.stderr.on 'data', (data) ->
            process.stderr.write data.toString()
        spawnInstance.stdout.on 'data', (data) ->
            print data.toString()
        spawnInstance.on 'exit', (code) ->
            callback?() if code is 0

watch = ->
    clientCoffees = getSpawn 'coffee', ['-w', '-c', '-b', '-o', 'public/javascripts', 'client']
    sharedCoffees = getSpawn 'coffee', ['-w', '-c', '-b', '-o', 'public/javascripts', 'shared']

    [clientCoffees, sharedCoffees].forEach (spawnInstance) ->
        spawnInstance.stderr.on 'data', (data) ->
          process.stderr.write data.toString()
        spawnInstance.stdout.on 'data', (data) ->
          print data.toString()

run = (debug=false) ->
    executable = if process.env.NODE_ENV is 'production' then 'coffee' else 'nodemon'
    args = []
    if process.env.NODE_ENV is 'development'
        args.push '--debug' if debug
        args.push '--watch'
        args.push 'server'
        args.push '--watch'
        args.push 'shared'
    args.push 'app.coffee'
    node = getSpawn executable, args
    node.stderr.on 'data', (data) ->
      process.stderr.write data.toString()
    node.stdout.on 'data', (data) ->
      print data.toString()
    node.on 'exit', (code) ->
        process.exit code

task 'build', 'Build public/javascripts from client/', ->
    build()

task 'watch', 'Watch client/ for changes', ->
    watch()

task 'dev', 'Watch server/client for changes and run node', ->
    process.env.NODE_ENV = 'development'
    watch()
    run()

# If you want to debug server side code, run this task and then:
# 1. Once the app is up and running it should say 'debugger listening on port 5858'
# 2. You can connect via terminal with `node debug localhost:5858`
task 'debug', 'Watch server/client for changes and debug node', ->
    process.env.NODE_ENV = 'development'
    watch()
    run(true)

task 'run', 'Build and run the project', ->
    process.env.NODE_ENV = 'production'
    build()
    run()
